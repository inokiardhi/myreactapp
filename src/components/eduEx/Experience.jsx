import React, { Fragment } from 'react'

function Experience() {
    return (
        <Fragment>

            <div id="3" class="experience py-5 mb-5">

                <div  class="container-md text-center ">
                    <h3 class="mt-3" style={{color: 'rgb(143, 189, 174)'}}>Sadly, I have never worked and I don't have IT background :'( </h3>
                    <h5 style={{color: 'rgb(243, 171, 76)'}}>but, it's Oookayy. That's why I join this bootcamp</h5>
                    <p class="skills" style={{color: 'rgb(192, 192, 192)'}}>but i got few skills though,</p>
                    
                    <div class=" d-flex justify-content-around">
                        <h3 class="rounded-pill w-25" style={{backgroundColor: 'rgb(57, 125, 202)', color: 'whitesmoke'}}>Photoshop</h3>
                        <h3 class="rounded-pill w-25" style={{backgroundColor: 'coral', color: 'whitesmoke'}}>Illustrator</h3>
                    </div>
                </div>

            </div>

        </Fragment>
    )
}

export default Experience
